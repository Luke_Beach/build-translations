const readYaml = require('./readYaml')
const yargs = require('yargs')
const path = require('path')
const fse = require('fs-extra')

const main = async ({ input, output }) => {
    const languages = await fse.readdir(input)

    if (languages.length === 0) {
        throw new Error(
            'No language directories were detected\n** Ensure that the input directory contains at least one folder containing yaml files **'
        )
    }

    console.log(
        `Building translation JSON files for ${languages.length} ${languages.length > 1 ? 'languages' : 'language'}\n...\n`
    )

    for (const language of languages) {
        const file = path.join(output, `${language}.json`)
        const json = await buildJsonForLanguage(path.join(input, language))
        await fse.outputFile(file, JSON.stringify(json, null, '\t'))
    }

    console.log(`Built ${languages.length} ${languages.length > 1 ? 'files' : 'file'} successfully`)
    console.log(`Wrote translations to ${output}`)
}

const buildJsonForLanguage = async languageDir => {
    const files = await fse.readdir(languageDir)
    const jsonList = await Promise.all(files.map(file => readYaml(path.join(languageDir, file))))
    return files.reduce(
        (json, file, index) => ({
            ...json,
            [file.replace('.yaml', '')]: jsonList[index]
        }),
        {}
    )
}

yargs
    .usage('Usage: $0 -i [input path] -o [output path]')
    .command('$0', '', () => {}, main)
    .alias('i', 'input')
    .nargs('i', 1)
    .describe(
        'i',
        'The path to the directory containing translation yaml files.\n\nThere should be a folder for each language, and inside each folder a yaml file for each logical group of translations.\nAll yaml files within a language directory will be combined into a singular json file, outputted to the provided output path'
    )
    .alias('o', 'output')
    .nargs('o', 1)
    .describe(
        'o',
        'The path to the directory where the built json files should be outputted\n\n*Warning* Any existing translation files with the same name in the output directory will be overwritten'
    )
    .demandOption(['i', 'o'])
    .help('h')
    .alias('h', 'help')
    .parse()
