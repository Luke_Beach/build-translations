const { PromiseContract } = require('@toryt/contracts-v')
const fse = require('fs-extra')
const yaml = require('js-yaml')

/**
 * @param  {string} lang
 * @returns {object} Labels
 */
const readYaml = new PromiseContract({
    pre: [path => fse.pathExistsSync(path)],
    post: [(path, result) => typeof result === 'object']
}).implementation(async function getLabels (path) {
    const contents = await fse.readFile(path, 'utf-8')
    return yaml.load(contents, {
        filename: path
    })
})

module.exports = readYaml
